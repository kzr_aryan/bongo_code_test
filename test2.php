<?php

//function definitions
function printDepth($data) {
    printDepthWithLevel($data,1);
}

function printDepthWithLevel($data,$level){
    foreach ($data as $key => $value){
        $dataType = gettype($value);
        if($dataType === "array" || $dataType === "object"){
            printDepthWithLevel($value,$level+1);
        }
        echo $key." ".$level."</br>";
    }
}

//test data
class Person
{
    public function __construct($first_name, $last_name, $father)
    {
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->father = $father;
    }
}
$person_a = new Person("User", "1", NULL);
$person_b = new Person("User", "2", $person_a);
$a = array(
    "key1" => 1,
    "key2" => array(
        "key3" => 1,
        "key4" => array(
            "key5" => 4,
            "User" => $person_b,
        ),
    ),
);

// run test
printDepth($a);

