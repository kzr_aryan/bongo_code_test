# bongo_code_test

##Test3
###time complexity
O(log(n))

To get the least common ancestor, this solution only have to traverse all parent of the given nodes. So the worst case scenario would be if both nodes are leaves of the tree whose common ancestor is root. In this case, the given solution will iterate for each parent of those input nodes. so that will be log(n) iteration if it's a binary tree. and for each iteration, there are two linear searches (in_array()). Which will usually take O(n) each, but as the number of searched item is gradually increasing. log(n) steps will have a different number of steps.
for iteration,
```
       1 => 1+1 =  2 steps
       2 => 2+2 = 4 steps
       ..
       log(n) => log(n)+log(n) = 2log(n)
```
So the time complexity for worst case, if the tree is a binary tree, will be O(log(n))
###space complexity
O(log(n))

Only need space for the parent nodes. For the worst case scenario, this solution needs two arrays with log(n) space.

###in_array($needle, $haystack)
This is a linear search of a item in an array.
```
foreach($haystack as $item){
    if($item == $needle) return true;
}
return false;
```
###gettype($data)
returns data type of input variable
