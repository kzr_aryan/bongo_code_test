<?php
// model
class Node{
    public $value;
    public $parent;

    public function __construct($value, $parent)
    {
        $this->value = $value;
        $this->parent = $parent;
    }
}

//functions definition
function lca($node1,$node2){

    $parentNodes1 = array($node1->value);
    $parentNodes2 = array($node2->value);
    $isSameNode = $node1->value == $node2->value;
    if($isSameNode){
        $result = $node1->value;
    }else{
        $isRoot = ($node1->parent === NULL) || ($node2->parent === NULL);
        if($isRoot){
            $result = ($node1->parent === NULL) ? $node1->value : $node2->value;
        }else{
            $isNode1InParentNodes2 = false;
            while(1){
                $isNode1InParentNodes2 = ($node1->parent !== NULL) ? in_array($node1->parent->value,$parentNodes2): false;
                $isNode2InParentNodes1 = ($node2->parent !== NULL) ? in_array($node2->parent->value,$parentNodes1): false;
                $isSiblings = ($node1->parent !== NULL) && ($node2->parent !== NULL) && $node1->parent->value === $node2->parent->value;
                if(!$isNode1InParentNodes2 && !$isNode2InParentNodes1 && !$isSiblings){
                    $parentNodes1[] = $node1->parent->value;
                    $parentNodes2[] = $node2->parent->value;
                    $node1 = $node1->parent;
                    $node2 = $node2->parent;
                }else{
                    break;
                }
            }
            $result = $isNode1InParentNodes2 ? $node1->parent->value : $node2->parent->value;
        }
    }
    return $result;
}

// test data
$node1 = new Node(1,NULL);
$node2 = new Node(2,$node1);
$node3 = new Node(3,$node1);
$node4 = new Node(4,$node2);
$node5 = new Node(5,$node2);
$node6 = new Node(6,$node3);
$node7 = new Node(7,$node3);
$node8 = new Node(8,$node4);
$node9 = new Node(9,$node4);

echo lca($node7,$node2);
