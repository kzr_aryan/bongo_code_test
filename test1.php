<?php

//functions definitions
function printDepth($data) {
    printDepthWithLevel($data,1);
}

function printDepthWithLevel($data,$level){
    foreach ($data as $key => $value){
        $dataType = gettype($value);
        if($dataType === "array"){
            printDepthWithLevel($value,$level+1);
        }
        echo $key." ".$level."</br>";
    }
}

//test data
$a = array (
    "key1" => 1,
    "key2" => array (
        "key3" => 1,
        "key4" => array (
            "key5" => 4
        ),
        50
    ),
);

//run test
printDepth($a);

//q1 do I need to print the output sorted by level as given example or any order?
//q2 do I exclude default numeric keys while printing? for example, array(1,2). should I print 0 1, 1 1?
